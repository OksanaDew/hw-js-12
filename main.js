//setTimeout позволяет вызвать функцию один раз через определённый интервал времени.
//setInterval позволяет вызывать функцию регулярно, повторяя вызов через определённый интервал времени.

//если в функцию setTimeout() передать нулевую задержку, то вызов функции произойдет настолько быстро,
//насколько это возможно, но это необязательно мгновенно.
//Потому что ланировщик вызовет функцию только после завершения выполнения текущего кода.

//Для setInterval функция остаётся в памяти до тех пор, пока не будет вызван clearInterval.
//Функция ссылается на внешнее лексическое окружение, поэтому пока она существует,
//внешние переменные существуют тоже. Они могут занимать больше памяти, чем сама функция.
//Поэтому, если регулярный вызов функции больше не нужен, то лучше отменить его,
//даже если функция очень маленькая.

let imgs = document.getElementsByClassName('image-to-show');
let wrapper = document.getElementById('images-wrapper');

const pauseButton = document.createElement('button');
pauseButton.textContent = 'Прекратить';

const resumeButton = document.createElement('button');
resumeButton.textContent = 'Возобновить показ';

wrapper.append(pauseButton);
wrapper.append(resumeButton);

showImages ();

function showImages () {
    let intervalId = setInterval(showImages, 4000);

    function toHideDisplays() {
        for (let i = 0; i < imgs.length; i++) {
            imgs[i].style.display = 'none';
        }
    }

    for (j = 0; j < imgs.length; j++) {
        debugger;
        (function (j) {
            pauseButton.addEventListener('click', onPauseClick);
            function onPauseClick(event) {
                clearTimeout(timerID);
                timerID = null;
                clearInterval(intervalId);
                intervalId = null;
            }

            resumeButton.addEventListener('click', onResumeClick);
            function onResumeClick(event) {
                timerID = setTimeout(function () {
                    toHideDisplays();
                    imgs[j].style.display = 'flex';
                }, 1000 * j);
                intervalId = setInterval(showImages, 4000);
            }

            let timerID = setTimeout(function () {
                toHideDisplays();
                imgs[j].style.display = 'flex';
            }, 1000 * j);
        }(j));
    }
}